# Home Automation Service #

### Base projects ###

* [Home Assistant](https://www.home-assistant.io/)
* [openHAB](https://www.openhab.org)

### IA projects ###

* [DeepStack](https://docs.deepstack.cc/)

### DVR projects ###

* [Shinobi](https://shinobi.video/) open com plugins pra detecção de objetos
* [BlueIris](https://blueirissoftware.com/) trial
* [Agent/iSpy](https://www.ispyconnect.com)